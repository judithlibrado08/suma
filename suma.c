#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv){

 
    static char numero1[50], numero2[50];
    static char aux[50];
    static int numEntero1[50], numEntero2[50];
    static int aux1[1], aux2[1];
    static int longitudNum1, longitudNum2, indice, acarreo;
    
    printf("\nSuma de dos numeros grandes tipo entero\n");
    printf("\nPrimer numero:\n");
    scanf("%s",numero1);

    printf("\nSegundo numero:\n");
    scanf("%s",numero2);
    printf("\n\nLa suma de [%s]+[%s] = \n ",numero1,numero2);
    longitudNum1 = strlen(numero1)-1;
    longitudNum2 = strlen(numero2)-1;

    
    if (longitudNum1 < longitudNum2)
    {
        while (longitudNum1 < longitudNum2)
        {
            for (int c = longitudNum1; c >= 0; c--){
                numero1[c+1] = numero1[c];
            }
            numero1[0] = '0';
            longitudNum1++;
        }
    }else
    {
        while (longitudNum1 > longitudNum2)
        {
            for (int c = longitudNum2; c >= 0; c--){
                numero2[c+1] = numero2[c];
            }
            numero2[0] = '0';
            longitudNum2++;
        }
    }
    
    printf("\n");

    
    for (int i = 0; i <= longitudNum1; i++){
        int valor = (int)numero1[i];
        numEntero1[i] = valor - 48;
        printf("%i ",numEntero1[i]);
    }
    printf("\n");
    for (int i = 0; i <= longitudNum2; i++){
        int valor = (int)numero2[i];
        numEntero2[i] = valor - 48;
        printf("%i ",numEntero2[i]);
    }

    
    int resindice = longitudNum1;
    int resultado[longitudNum1+1];
    int contador = 0;
    for (indice = longitudNum1; 0 <= indice; indice--)
    {   
        aux1[0] = numEntero1[indice];
        aux2[0] = numEntero2[indice];
        int sum = aux1[0] + aux2[0] + acarreo;
        acarreo = 0;
        if (sum > 9)
        {
            acarreo = 1;
            sum = sum % 10;
        }        
        
        resultado[resindice] = sum;
        resindice--;
        contador++;
        if (indice == 0 && acarreo > 0)
        {
            for (int c = contador; c >= 0; c--){
                resultado[c+1] = resultado[c];
            }
            resultado[0] = acarreo;
            contador++;
        }
        
    }
    
    
    int tamanioDelArreglo = sizeof(resultado);
    int tamanioDeLaPrimeraVariableDelArreglo = sizeof(resultado[0]);
    int longitud = tamanioDelArreglo / tamanioDeLaPrimeraVariableDelArreglo;
    
    
    printf("\nResultado: \n");
    for (int i = 0; i < contador; i++)
    {
        printf(/*"pos[%i]-> "*/"%i ",resultado[i]);
    }
    
    
    printf("\n");
    return 0;
}
